#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import os
import sys
import argparse

from machinelearning.Dataset import Dataset
from machinelearning.ConfusionMatrix import ConfusionMatrix

from machinelearning.classifier.NaiveBayes import NaiveBayes
from machinelearning.classifier.ILA import ILA
from machinelearning.classifier.KNN import KNN

from machinelearning.classifier.ensembler.Boosting import Boosting
from machinelearning.classifier.ensembler.Bagging import Bagging

# Global defines:
APP_NAME = "clsTester"
APP_VER = "1.4"

def main():
    # Define different arguments constants:
    CMD_ARG_DATA = "data"
    CMD_ARG_FOLDS = "folds"
    CMD_ARG_NON_STRATIFIED = "non-stratified"
    CMD_ARG_VERBOSE = "verbose"
    CMD_ARG_RANGE_DISCRETIZE = "range-discretize"
    CMD_ARG_AMOUNT_DISCRETIZE = "amount-discretize"
    CMD_ARG_CLASSIFIER = "classifier"
    CMD_ARG_ENSEMBLER = "ensembler"
    
    VAL_OPT_CLASSIFIER = [
        ("bayes", NaiveBayes),
        ("ila", ILA),
        ("knn", KNN)
    ]
    VAL_OPT_ENSEMBLER = [
        ("boosting", Boosting),
        ("bagging", Bagging)
    ]
    
    VAL_DEF_FOLDS = 10
    VAL_DEF_RANGE_DISCRETIZE = None
    VAL_DEF_AMOUNT_DISCRETIZE = None
    VAL_DEF_CLASSIFIER = VAL_OPT_CLASSIFIER[0][0]
    
    SPLITTER = "----------------------------------------------------------"
    
    # Create arguments parser instance:
    cmdArgParser = argparse.ArgumentParser(
        prog=APP_NAME,
        version=APP_VER,
        description="Application for testing custom implementations of different classifiers."
    )
    
    # Add arguments:
    cmdArgParser.add_argument(
        "--"+CMD_ARG_DATA, "--"+CMD_ARG_DATA[0].upper(), help="Path to data file in CSV format",
        action="store", dest=CMD_ARG_DATA, required=True
    )
    cmdArgParser.add_argument(
        "--"+CMD_ARG_FOLDS, "-"+CMD_ARG_FOLDS[0].upper(), help="Amount of folds for cross-validation",
        action="store", dest=CMD_ARG_FOLDS, type=int, default=VAL_DEF_FOLDS
    )
    cmdArgParser.add_argument(
        "--"+CMD_ARG_NON_STRATIFIED, "-"+CMD_ARG_NON_STRATIFIED[0].upper(), help="Use non stratified cross validation",
        action="store_false", dest=CMD_ARG_NON_STRATIFIED, default=True
    )
    cmdArgParser.add_argument(
        "--"+CMD_ARG_VERBOSE, "-"+CMD_ARG_VERBOSE[0].upper(), help="Print all processed data",
        action="store_true", dest=CMD_ARG_VERBOSE, default=False
    )
    
    cmdArgDiscretizeGroup = cmdArgParser.add_mutually_exclusive_group()
    cmdArgDiscretizeGroup.add_argument(
        "--"+CMD_ARG_RANGE_DISCRETIZE, "-"+CMD_ARG_RANGE_DISCRETIZE[0].upper(), help="Discretize in equal ranges",
        action="store", dest=CMD_ARG_RANGE_DISCRETIZE, type=int, default=VAL_DEF_RANGE_DISCRETIZE
    )
    cmdArgDiscretizeGroup.add_argument(
        "--"+CMD_ARG_AMOUNT_DISCRETIZE, "-"+CMD_ARG_AMOUNT_DISCRETIZE[0].upper(), help="Discretize in equal amounts",
        action="store", dest=CMD_ARG_AMOUNT_DISCRETIZE, type=int, default=VAL_DEF_AMOUNT_DISCRETIZE
    )
    
    cmdArgParser.add_argument(
        "--"+CMD_ARG_CLASSIFIER, "-"+CMD_ARG_CLASSIFIER[0].upper(), help="Choose classifier",
        action="store", dest=CMD_ARG_CLASSIFIER, default=VAL_DEF_CLASSIFIER
    )
    
    cmdArgParser.add_argument(
        "--"+CMD_ARG_ENSEMBLER, "-"+CMD_ARG_ENSEMBLER[0].upper(), help="Choose ensembler",
        action="store", dest=CMD_ARG_ENSEMBLER, default=None
    )
    
    args = cmdArgParser.parse_args()
    
    dataPath = os.path.abspath(getattr(args, CMD_ARG_DATA))
    if not os.path.exists(dataPath):
        print("Path does not exist:\n'%s'" % dataPath)
        sys.exit(1)
    if not os.path.isfile(dataPath):
        print("Path must be a file:\n'%s'" % dataPath)
        sys.exit(1)
    
    folds = getattr(args, CMD_ARG_FOLDS)
    if folds < 2:
        print("There must be at least 2 folds!")
        sys.exit(1)
    
    stratified = getattr(args, CMD_ARG_NON_STRATIFIED)
    verbose = getattr(args, CMD_ARG_VERBOSE)
    
    rangeDiscretize = getattr(args, CMD_ARG_RANGE_DISCRETIZE)
    amountDiscretize = getattr(args, CMD_ARG_AMOUNT_DISCRETIZE)
    
    classifierName = getattr(args, CMD_ARG_CLASSIFIER).split("/")
    classifierName[0] = classifierName[0].lower()
    if not classifierName[0] in dict(VAL_OPT_CLASSIFIER).keys():
        print("Classifier must be one of:\n'%s'" %(", ".join(dict(VAL_OPT_CLASSIFIER).keys())))
        sys.exit(1)
    classifier = dict(VAL_OPT_CLASSIFIER)[classifierName[0]]
    classifierParams = dict([item.split("=") for item in classifierName[1:]])
    classifierParams[CMD_ARG_VERBOSE] = verbose
    
    ensembler = None
    ensemblerParams = None
    if not (getattr(args, CMD_ARG_ENSEMBLER) is None):
        ensemblerName = getattr(args, CMD_ARG_ENSEMBLER).split("/")
        ensemblerName[0] = ensemblerName[0].lower()
        if not ensemblerName[0] in dict(VAL_OPT_ENSEMBLER).keys():
            print("Ensembler must be one of:\n'%s'" %(", ".join(dict(VAL_OPT_ENSEMBLER).keys())))
            sys.exit(1)
        ensembler = dict(VAL_OPT_ENSEMBLER)[ensemblerName[0]]
        ensemblerParams = dict([item.split("=") for item in ensemblerName[1:]])
        ensemblerParams[CMD_ARG_VERBOSE] = verbose
    
    data = Dataset.loadFormCSV(dataPath)
    if rangeDiscretize is not None:
        data = Dataset.discretizeInEqualRanges(data, rangeDiscretize)
    if amountDiscretize is not None:
        data = Dataset.discretizeInEqualParts(data, amountDiscretize)
    
    print(SPLITTER)
    print("Data loaded form file:\n'%s'" % dataPath)
    print(SPLITTER)
    print("Data records count: '%s'" % str(len(data)))
    print("Folds count to split in: '%s'" % str(folds))
    print("Approximate fold elements count: '%s'" % str(len(data)/folds))
    print("Is cross validation split stratified: '%s'" % stratified)
    
    if verbose:
        print(SPLITTER)
        for clsName, clsRecords in Dataset.splitByClass(data).items():
            print("Amount of records in class '%s': '%s'" % (str(clsName), str(len(clsRecords))))
    
    confMatrixes = []
    for dTrain, dTest in Dataset.splitForCV(data, folds, stratified):
        cm = None
        if ensembler is None:
            cls = classifier.fit(dTrain, **classifierParams)
            cm = cls.predict(dTest)
            confMatrixes.append(cm)
        else:
            ens = ensembler.fit(dTrain, classifier, classifierParams, **ensemblerParams)
            cm = ens.predict(dTest)
            confMatrixes.append(cm)
        if verbose:
            print(SPLITTER)
            print(cm)
            print("")
            print(["Accuracy", "Precision", "Recall", "FScore"])
            print([cm.calcAccuracy(), cm.calcPrecision(), cm.calcRecall(), cm.calcFScore()])
    
    confMatrix = ConfusionMatrix.sum(confMatrixes)
    
    print(SPLITTER)
    print("Over all confusion matrix:")
    print(confMatrix)
    print("")
    print(["Accuracy", "Precision", "Recall", "FScore"])
    print([confMatrix.calcAccuracy(), confMatrix.calcPrecision(), confMatrix.calcRecall(), confMatrix.calcFScore()])
##

if __name__=="__main__":
    main()
##
