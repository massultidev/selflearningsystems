#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

from machinelearning.ConfusionMatrix import ConfusionMatrix

class ClassifierBase():
    
    def __init__(self, **kwargs):
        for name, value in kwargs.items():
            setattr(self, name, value)
    ##
    
    @classmethod
    def fit(cls, trainDataset, **kwargs):
        """Returns classifier instance trained for provided data set.
        
        Arguments:
            trainDataset (list): Training data.
        
        Return:
            ClassifierBase: Instance of class derived from ClassifierBase class.
        """
        return cls(trainDataset, **kwargs)
    ##
    
    def predict(self, testDataset):
        """Returns confusion matrix of predictions made for provided test data set.
        
        Arguments:
            testDataset (list): Test data.
        
        Return:
            list: Confusion matrix.
        """
        cm = ConfusionMatrix()
        for record in testDataset:
            clsLblTrue = record[-1]
            clsLblPred = self._makePrediction(record)
            cm.addPrediction(clsLblTrue, clsLblPred)
        return cm
    ##
    
    def _makePrediction(self, record):
        """Should return predicted label.
        
        Arguments:
            record (list): Record with class attribute at the end (can be None).
        
        Return:
            object: Predicted label.
        """
        raise NotImplemented("This method must be overridden in subclasses!")
##
