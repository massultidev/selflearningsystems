#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import math

from machinelearning.classifier.ClassifierBase import ClassifierBase

class KNN(ClassifierBase):
    
    class Vote():
        WEIGHTED = "weighted"
        MAJORITY = "majority"
        MEAN = "mean"
    
    class Distance():
        EUCLIDEAN = "euclidean"
        MANHATTAN = "manhattan"
    
    def __init__(self, trainDataset, **kwargs):
        ClassifierBase.__init__(self, **kwargs)
        
        PARAM_K = "k"
        PARAM_VOTE = "vote"
        PARAM_DISTANCE = "dist"
        
        if not hasattr(self, PARAM_K):
            self._k = 5
        else:
            self._k = int(getattr(self, PARAM_K))
        
        if not hasattr(self, PARAM_VOTE):
            self._vote = KNN.Vote.WEIGHTED
        else:
            self._vote = getattr(self, PARAM_VOTE).lower()
            if self._vote not in self._getParams(KNN.Vote):
                raise Exception("Unknown voting method requested!")
        
        if not hasattr(self, PARAM_DISTANCE):
            self._dist = KNN.Distance.EUCLIDEAN
        else:
            self._dist = getattr(self, PARAM_DISTANCE).lower()
            if self._dist not in self._getParams(KNN.Distance):
                raise Exception("Unknown distance measure requested!")
        
        self._trainDataset = trainDataset
        
        print("KNN Parameters: [%s=%d] [%s=%s] [%s=%s]" % (
                PARAM_K, self._k,
                PARAM_VOTE, self._vote,
                PARAM_DISTANCE, self._dist
        ))
    ##
    
    def _makePrediction(self, record):
        distances = [(self._calcDistance(record, item), item) for item in self._trainDataset]
        distances.sort(key=lambda x: x[0])
        neighbors = [distances[i] for i in range(self._k)]
        return self._makeVoting(neighbors)
    ##
    
    def _makeVoting(self, neighbors):
        if self._vote == KNN.Vote.WEIGHTED:
            neighborsWeights = []
            for idx in range(len(neighbors)):
                if neighbors[-1][0] == neighbors[idx][0]:
                    neighborsWeights.append((1, neighbors[idx][1]))
                else:
                    neighborsWeights.append((
                        ((neighbors[-1][0] - neighbors[idx][0])/(neighbors[-1][0] - neighbors[0][0])) * (1.0/(idx+1)),
                        neighbors[idx][1]
                    ))
            clsWeights = self._splitClsNeighbors(neighborsWeights)
            return max([(item[0], len(item[1])) for item in clsWeights.items()], key=lambda x: x[1])[0]
        
        if self._vote == KNN.Vote.MAJORITY:
            clsSplit = self._splitClsNeighbors(neighbors)
            return max([(item[0], len(item[1])) for item in clsSplit.items()], key=lambda x: x[1])[0]
        
        if self._vote == KNN.Vote.MEAN:
            clsSplit = self._splitClsNeighbors(neighbors)
            clsWeightsMean = {}
            for clsName, clsNeighbors in clsSplit.items():
                weights = [item[0] for item in clsNeighbors]
                clsWeightsMean[clsName] = sum(weights)/len(weights)
            return min(clsWeightsMean.items(), key=lambda x: x[1])[0]
        
        else:
            raise Exception("Unknown voting method requested!")
    ##
    
    @classmethod
    def _splitClsNeighbors(cls, neighbors):
        clsSplit = {}
        for neighbor in neighbors:
            clsName = neighbor[1][-1]
            if clsName not in clsSplit:
                clsSplit[clsName] = [neighbor]
            else:
                clsSplit[clsName].append(neighbor)
        return clsSplit
    ##
    
    @classmethod
    def _getParams(cls, pType):
        return [getattr(pType, key) for key in pType.__dict__.keys() if not key.startswith("_")]
    ##
    
    def _calcDistance(self, record1, record2):
        if self._dist == KNN.Distance.EUCLIDEAN:
            return float(math.sqrt(sum([pow(record1[i] - record2[i], 2) for i in range(len(record1)-1)])))
        
        if self._dist == KNN.Distance.MANHATTAN:
            return float(sum([abs(record1[i] - record2[i]) for i in range(len(record1)-1)]))
        
        else:
            raise Exception("Unknown distance calculation algorithm!")
##
