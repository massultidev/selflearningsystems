#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import sys
import math

from machinelearning.classifier.ClassifierBase import ClassifierBase
from machinelearning.Dataset import Dataset

class NaiveBayes(ClassifierBase):
    
    def __init__(self, trainDataset, **kwargs):
        ClassifierBase.__init__(self, **kwargs)
        
        self._dataStatistics = {}
        
        for clsName, clsRecords in Dataset.splitByClass(trainDataset).items():
            attrs = zip(*clsRecords)
            attrs.pop(-1)
            self._dataStatistics[clsName] = [(self._calcMean(attr), self._calcStdDeviation(attr)) for attr in attrs]
    ##
    
    def getDataStatistics(self):
        return self._dataStatistics # Might be useful for verbose output.
    ##
    
    def _makePrediction(self, record):
        clsProbabilities = self._calcClsProbabilities(record)
        best = clsProbabilities.items()[0]
        for clsName, clsProbability in clsProbabilities.items():
            if best[1] < clsProbability:
                best = (clsName, clsProbability)
        return best[0]
    ##
    
    def _calcClsProbabilities(self, record):
        clsProbabilities = {}
        for clsName, attrStatistics in self._dataStatistics.items():
            if len(attrStatistics) == 0:
                clsProbabilities[clsName] = 0
                continue
            clsProbabilities[clsName] = 1
            for idx in range(len(attrStatistics)):
                val = record[idx]
                mean, stdDeviation = attrStatistics[idx]
                clsProbabilities[clsName] *= self._calcProbability(val, mean, stdDeviation)
        return clsProbabilities
    ##
    
    @classmethod
    def _calcProbability(cls, val, mean, stdDeviation):
        # Use gaussian probability density function in order to estimate probability:
        if stdDeviation == 0.0:
            stdDeviation = 1/float(sys.maxint)
        return (1/(math.sqrt(2*math.pi)*stdDeviation)) * math.exp(-(math.pow(val-mean,2)/(2*math.pow(stdDeviation,2))))
    ##
    
    @classmethod
    def _calcMean(cls, lst):
        return sum(lst)/float(len(lst))
    @classmethod
    def _calcStdDeviation(cls, lst):
        mean = cls._calcMean(lst)
        denominator = float(len(lst)-1)
        if denominator == 0.0:
            denominator = 1/float(sys.maxint)
        variance = sum([pow(val-mean, 2) for val in lst])/denominator
        return math.sqrt(variance)
##
