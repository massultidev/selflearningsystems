#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import itertools
import collections

from machinelearning.classifier.ClassifierBase import ClassifierBase
from machinelearning.Dataset import Dataset

class ILA(ClassifierBase):
    
    def __init__(self, trainDataset, **kwargs):
        ClassifierBase.__init__(self, **kwargs)
        
        self._clsRules = None
        
        attrCount = len(zip(*trainDataset))-1
        
        clsSplit = Dataset.splitByClass(trainDataset)
        self._clsRules = {clsLabel:[] for clsLabel in clsSplit}
        for clsName, clsRecords in clsSplit.items():
            matchedRecordsIndexes = []
            attrCmbCount = 1
            
            # Loop until all records are matched:
            while len(matchedRecordsIndexes) != len(clsRecords):
                if attrCmbCount > attrCount:
                    break
                maxCmbsPerCount = []
                
                # Iterate all possible combinations:
                for cmbx in itertools.combinations(range(attrCount), attrCmbCount):
                    uniqueCmbs = collections.OrderedDict()
                    
                    # Find unique values for current combination:
                    for idx in range(len(clsRecords)):
                        if idx in matchedRecordsIndexes: # Ignore record if already matched.
                            continue
                        record = clsRecords[idx]
                        
                        # Check if combination present in another classes:
                        isMatched = False
                        for lbl, items in clsSplit.items():
                            if lbl == clsName: # Ignore records from the same class.
                                continue
                            for item in items:
                                matchedAttrs = []
                                for index in list(cmbx):
                                    if item[index] == record[index]:
                                        matchedAttrs.append(True)
                                    else:
                                        matchedAttrs.append(False)
                                if False not in matchedAttrs:
                                    isMatched = True
                                if isMatched:
                                    break
                            if isMatched:
                                break
                        
                        if not isMatched: # Mark as unique if not in other classes.
                            val = [record[i] if i in list(cmbx) else None for i in range(len(record)-1)]
                            lbl = str(val)
                            if lbl not in uniqueCmbs.keys():
                                uniqueCmbs[lbl] = [val, 1, [idx]]
                            else:
                                uniqueCmbs[lbl][1] += 1
                                uniqueCmbs[lbl][2].append(idx)
                    
                    # Get unique combination values that occurred most often:
                    if len(uniqueCmbs) > 0:
                        maxCount = max(zip(*uniqueCmbs.values())[1])
                        for item in uniqueCmbs.values():
                            if item[1] == maxCount:
                                maxCmbsPerCount.append(item)
                                break
                
                # Get most often occurred per attributes combination count:
                if len(maxCmbsPerCount) > 0:
                    maxCount = max(zip(*maxCmbsPerCount)[1])
                    for item in maxCmbsPerCount:
                        if item[1] == maxCount:
                            self._clsRules[clsName].append(item[0]) # Create rule.
                            matchedRecordsIndexes += item[2]
                            break
                else:
                    attrCmbCount += 1
    ##
    
    def _makePrediction(self, record):
        clsPred = {clsName:0 for clsName in self._clsRules.keys()}
        for clsName, clsRules in self._clsRules.items():
            for rule in clsRules:
                matchedAttrs = [record[idx] == rule[idx] if rule[idx] is not None else True for idx in range(len(rule))]
                if False not in matchedAttrs:
                    clsPred[clsName] += 1
        maxCount = max(zip(*clsPred.items())[1])
        for clsName, clsCount in clsPred.items():
            if clsCount == maxCount:
                return clsName
##
