#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import random

from machinelearning.classifier.ensembler.EnsemblerBase import EnsemblerBase
from machinelearning.classifier.ensembler.EnsemblerBase import EnsemblerBaseParams

class Bagging(EnsemblerBase):
    
    def __init__(
        self,
        trainDataset,
        classifierType,
        classifierParams=EnsemblerBaseParams.DEFAULT_CLASSIFIER_PARAMS,
        classifierAmount=EnsemblerBaseParams.DEFAULT_CLASSIFIER_AMOUNT,
        sampleSizeRatio=EnsemblerBaseParams.DEFAULT_SAMPLE_SIZE_RATIO,
        sampleWithDuplicates=EnsemblerBaseParams.DEFAULT_SAMPLE_DUPLICATES,
        **kwargs
    ):
        EnsemblerBase.__init__(
            self,
            trainDataset,
            classifierType,
            classifierParams,
            classifierAmount,
            sampleSizeRatio,
            sampleWithDuplicates,
            **kwargs
        )
        
        self._classifiers = []
        
        classifierAmount = int(classifierAmount)
        sampleSizeRatio = float(sampleSizeRatio)
        sampleWithDuplicates = bool(sampleWithDuplicates)
        
        for i in range(classifierAmount):
            VERBOSE = "verbose"
            if VERBOSE in kwargs.keys() and kwargs[VERBOSE] == True:
                print("Training classifier %d..." %(i+1))
            sample = self._genSample(trainDataset, sampleSizeRatio, sampleWithDuplicates)
            classifier = classifierType.fit(sample, **classifierParams)
            self._classifiers.append(classifier)
    ##
    
    @classmethod
    def _genSample(cls, dataset, ratio, duplicates):
        sample = []
        sampleSize = round(len(dataset) * ratio)
        datasetShallowCopy = list(dataset)
        while len(sample) < sampleSize:
            idx = random.randrange(len(datasetShallowCopy))
            sample.append(datasetShallowCopy[idx])
            if duplicates == False:
                datasetShallowCopy.pop(idx)
        return sample
    ##
    
    def _makePrediction(self, record):
        predictions = {}
        for classifier in self._classifiers:
            clsLblPred = classifier._makePrediction(record)
            if clsLblPred not in predictions.keys():
                predictions[clsLblPred] = 1
            else:
                predictions[clsLblPred] += 1
        return max(predictions.items(), key=lambda x: x[1])[0]
##
