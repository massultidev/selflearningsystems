#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

from machinelearning.ConfusionMatrix import ConfusionMatrix

from machinelearning.classifier.ClassifierBase import ClassifierBase

class EnsemblerBaseParams():
    DEFAULT_CLASSIFIER_PARAMS = {}
    DEFAULT_CLASSIFIER_AMOUNT = 3
    DEFAULT_SAMPLE_SIZE_RATIO = 1.0
    DEFAULT_SAMPLE_DUPLICATES = True

class EnsemblerBase():
    
    def __init__(
        self,
        trainDataset,
        classifierType,
        classifierParams=EnsemblerBaseParams.DEFAULT_CLASSIFIER_PARAMS,
        classifierAmount=EnsemblerBaseParams.DEFAULT_CLASSIFIER_AMOUNT,
        sampleSizeRatio=EnsemblerBaseParams.DEFAULT_SAMPLE_SIZE_RATIO,
        sampleWithDuplicates=EnsemblerBaseParams.DEFAULT_SAMPLE_DUPLICATES,
        **kwargs
    ):
        if not issubclass(classifierType, ClassifierBase):
            raise Exception("Parameter 'classifierType' must derive of 'ClassifierBase'!")
        print("----------------------------------------------------------")
        print("Chosen classifier type: %s" %(str(classifierType)))
        print("Chosen classifier amount: %s" %(classifierAmount))
        print("Chosen sample size ratio: %s" %(sampleSizeRatio))
        for name, value in kwargs.items():
            setattr(self, name, value)
    ##
    
    @classmethod
    def fit(
        cls,
        trainDataset,
        classifierType,
        classifierParams=EnsemblerBaseParams.DEFAULT_CLASSIFIER_PARAMS,
        classifierAmount=EnsemblerBaseParams.DEFAULT_CLASSIFIER_AMOUNT,
        sampleSizeRatio=EnsemblerBaseParams.DEFAULT_SAMPLE_SIZE_RATIO,
        sampleWithDuplicates=EnsemblerBaseParams.DEFAULT_SAMPLE_DUPLICATES,
        **kwargs
    ):
        return cls(
            trainDataset,
            classifierType,
            classifierParams,
            classifierAmount,
            sampleSizeRatio,
            sampleWithDuplicates,
            **kwargs
        )
    ##
    
    def predict(self, testDataset):
        cm = ConfusionMatrix()
        for record in testDataset:
            clsLblTrue = record[-1]
            clsLblPred = self._makePrediction(record)
            cm.addPrediction(clsLblTrue, clsLblPred)
        return cm
    ##
    
    def _makePrediction(self, record):
        raise NotImplemented("This method must be overridden in subclasses!")
##
