#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import sys
import math
import random

from machinelearning.classifier.ensembler.EnsemblerBase import EnsemblerBase
from machinelearning.classifier.ensembler.EnsemblerBase import EnsemblerBaseParams

class Boosting(EnsemblerBase):
    
    def __init__(
        self,
        trainDataset,
        classifierType,
        classifierParams=EnsemblerBaseParams.DEFAULT_CLASSIFIER_PARAMS,
        classifierAmount=EnsemblerBaseParams.DEFAULT_CLASSIFIER_AMOUNT,
        sampleSizeRatio=EnsemblerBaseParams.DEFAULT_SAMPLE_SIZE_RATIO,
        sampleWithDuplicates=EnsemblerBaseParams.DEFAULT_SAMPLE_DUPLICATES,
        **kwargs
    ):
        EnsemblerBase.__init__(
            self,
            trainDataset,
            classifierType,
            classifierParams,
            classifierAmount,
            sampleSizeRatio,
            sampleWithDuplicates,
            **kwargs
        )
        
        self._classifiers = []
        self._stages = []
        
        classifierAmount = int(classifierAmount)
        sampleSizeRatio = float(sampleSizeRatio)
        sampleWithDuplicates = bool(sampleWithDuplicates)
        
        N = len(trainDataset)
        initWeight = 1.0/N
        dataset = [[initWeight, record] for record in trainDataset]
        for i in range(classifierAmount):
            VERBOSE = "verbose"
            if VERBOSE in kwargs.keys() and kwargs[VERBOSE] == True:
                print("Training classifier %d..." %(i+1))
            sample = self._genSample(dataset, sampleSizeRatio, sampleWithDuplicates)
            classifier = classifierType.fit(sample, **classifierParams)
            
            predictions = []
            for _, record in dataset:
                clsLblTrue = record[-1]
                clsLblPred = classifier._makePrediction(record)
                predictions.append(int(not (clsLblPred == clsLblTrue)))
            
            # Calculate missclassification rate value:
            error = sum([dataset[i][0] * predictions[i] for i in range(len(dataset))]) / sum([dataset[i][0] for i in range(len(dataset))])
            if error == 0.0:
                error = 1/float(sys.maxint)
            
            # Calculate stage/ALPHA factor value:
            stage = math.log((1.0-error) / error)
            
            # Update weights:
            for i in range(len(dataset)):
                dataset[i][0] *= math.exp(stage * predictions[i])
            
            self._stages.append(stage)
            self._classifiers.append(classifier)
    ##
    
    @classmethod
    def _genSample(cls, dataset, ratio, duplicates):
        sample = []
        sampleSize = int(round(len(dataset) * ratio))
        datasetShallowCopy = list(dataset)
        while len(sample) < sampleSize:
            idxes = [(datasetShallowCopy[i][0], i) for i in range(len(datasetShallowCopy))]
            idx = None
            while idx is None:
                idx = cls._randomWeighted(idxes)
            sample.append(datasetShallowCopy[idx][1])
            if duplicates == False:
                datasetShallowCopy.pop(idx)
        return sample
    ##
    
    @classmethod
    def _randomWeighted(cls, weightValuePairs):
        total = sum(pair[0] for pair in weightValuePairs)
        r = random.uniform(0.0, total)
        for (weight, value) in weightValuePairs:
            r -= weight
            if r <= 0:
                return value
    ##
    
    def _makePrediction(self, record):
        predictions = {}
        for i in range(len(self._classifiers)):
            classifier = self._classifiers[i]
            stage = self._stages[i]
            clsLblPred = classifier._makePrediction(record)
            if clsLblPred not in predictions.keys():
                predictions[clsLblPred] = stage
            else:
                predictions[clsLblPred] += stage
        return max(predictions.items(), key=lambda x: x[1])[0]
##
