#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import ast

class ConfusionMatrix():
    
    def __init__(self):
        self._matrix = [[None]]
    ##
    
    @staticmethod
    def sum(lst):
        item = lst[0]
        for idx in range(1, len(lst)):
            item += lst[idx]
        return item
    ##
    
    def addPrediction(self, lblTrue, lblPred, count=1):
        if lblTrue not in self._matrix[0]:
            self._addLabel(lblTrue)
        if lblPred not in self._matrix[0]:
            self._addLabel(lblPred)
        idxTrue = self._getLabelIndex(lblTrue)
        idxPred = self._getLabelIndex(lblPred)
        self._matrix[idxTrue][idxPred] += count
    ##
    
    def calcAccuracy(self, clsLabel=None): # Correct predictions divided by trials.
        if clsLabel is not None:
            lbls = [clsLabel]
        else:
            lbls = self._matrix[0][1:]
        
        results = []
        for lbl in lbls:
            tp = self._getTP(lbl)
            tn = self._getTN(lbl)
            fp = self._getFP(lbl)
            fn = self._getFN(lbl)
            if (tp+tn+fn+fp) > 0:
                results.append((tp+tn)/(tp+tn+fn+fp))
            else:
                results.append(1)
        
        return sum(results)/len(results)
    ##
    
    def calcPrecision(self, clsLabel=None):
        if clsLabel is not None:
            lbls = [clsLabel]
        else:
            lbls = self._matrix[0][1:]
        
        results = []
        for lbl in lbls:
            tp = self._getTP(lbl)
            fp = self._getFP(lbl)
            if fp == 0: # If FP=0 => Precision=1, since there was no false positive results.
                results.append(1.0)
                continue
            results.append(tp/(tp+fp))
        
        return sum(results)/len(results)
    ##
    
    def calcRecall(self, clsLabel=None):
        if clsLabel is not None:
            lbls = [clsLabel]
        else:
            lbls = self._matrix[0][1:]
        
        results = []
        for lbl in lbls:
            tp = self._getTP(lbl)
            fn = self._getFN(lbl)
            if fn == 0: # If FN=0 => Recall=1, since all of true positives were discovered.
                results.append(1.0)
                continue
            results.append(tp/(tp+fn))
        
        return sum(results)/len(results)
    ##
    
    def calcFScore(self, clsLabel=None):
        if clsLabel is not None:
            lbls = [clsLabel]
        else:
            lbls = self._matrix[0][1:]
        
        results = []
        for lbl in lbls:
            tp = self._getTP(lbl)
            fp = self._getFP(lbl)
            fn = self._getFN(lbl)
            if tp == 0 and fp == 0 and fn == 0:
                results.append(1.0)
                continue
            results.append((2*tp)/(2*tp+fp+fn))
        
        return sum(results)/len(results)
    ##
    
    def _addLabel(self, lbl):
        self._matrix.append([lbl]+[0]*(len(self._matrix[0])-1))
        self._matrix[0].append(lbl)
        for i in range(1, len(self._matrix)):
            self._matrix[i] += [0]
    ##
    
    def _getLabelIndex(self, lbl):
        for idx in range(1, len(self._matrix[0])):
            if lbl == self._matrix[0][idx]:
                return idx
    ##
    
    def _getTP(self, lbl):
        # Get vertical:
        idx = self._getLabelIndex(lbl)
        tp = self._matrix[idx][idx]
        return float(tp)
    def _getTN(self, lbl):
        # Cross out and sum the rest:
        idx = self._getLabelIndex(lbl)
        tn = 0
        for i in range(1, len(self._matrix)):
            if i == idx:
                continue
            for j in range(1, len(self._matrix[i])):
                if j == idx:
                    continue
                tn += self._matrix[i][j]
        return float(tn)
    def _getFP(self, lbl): # Type I error.
        # Cross out TP and sum column:
        idx = self._getLabelIndex(lbl)
        fp = 0
        for i in range(1, len(self._matrix)):
            if i == idx:
                continue
            fp += self._matrix[i][idx]
        return float(fp)
    def _getFN(self, lbl): # Type II error.
        # Cross out TP and sum row:
        idx = self._getLabelIndex(lbl)
        fn = 0
        for i in range(1, len(self._matrix[idx])):
            if i == idx:
                continue
            fn += self._matrix[idx][i]
        return float(fn)
    ##
    
    def __add__(self, other):
        cm = ConfusionMatrix()
        preds = {}
        for i in range(1, len(self._matrix)):
            for j in range(1, len(self._matrix[i])):
                lbl = str((self._matrix[i][0], self._matrix[0][j]))
                val = self._matrix[i][j]
                if lbl not in preds.keys():
                    preds[lbl] = val
                else:
                    preds[lbl] += val
        for i in range(1, len(other._matrix)):
            for j in range(1, len(other._matrix[i])):
                lbl = str((other._matrix[i][0], other._matrix[0][j]))
                val = other._matrix[i][j]
                if lbl not in preds.keys():
                    preds[lbl] = val
                else:
                    preds[lbl] += val
        for lbl, val in preds.items():
            lblTuple = ast.literal_eval(lbl)
            cm.addPrediction(lblTuple[0], lblTuple[1], val)
        return cm
    ##
    
    def __str__(self):
        out = "["
        for idx in range(len(self._matrix)):
            out += str(self._matrix[idx])
            if idx < len(self._matrix)-1:
                out += ",\n"
        out += "]"
        return out
    def __repr__(self):
        return str(self)
##
