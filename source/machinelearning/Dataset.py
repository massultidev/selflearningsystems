#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
#@Author: Paweł Grochowski
#

import csv
import random

class Dataset():
    
    @staticmethod
    def loadFormCSV(path):
        with open(path, "rb") as f: # Open file for reading in binary mode.
            data = list(csv.reader(f)) # Properly read csv (to avoid split problems).
        for i in range(len(data)):
            if len(data[i]) == 0:
                data.pop(i) # Remove empty.
                continue
            clsLabel = data[i][-1] # Save class label, because we will omit it later.
#             data[i] = [float(data[i][j]) if isinstance(data[i][j], int) else data[i][j] for j in range(len(data[i])-1)]
            # Cast all values to float (but omit class label - might not be numeric):
            dataRow = []
            for j in range(len(data[i])-1):
                try:
                    dataRow.append(float(data[i][j]))
                except:
                    dataRow.append(data[i][j])
            data[i] = dataRow
            data[i].append(clsLabel)
        return data
    ##
    
    @staticmethod
    def discretizeInEqualParts(data, count):
        splitStep = 1.0/float(count)
        splitValues = [val*splitStep for val in range(1, count+1)]
        splitRanges = []
        for attr in zip(*data)[:-1]:
            #Warning: One element less:
            attrRanges = [max(lst) for lst in Dataset._splitEqually(sorted(list(attr)), count)[:-1]]
            splitRanges.append(attrRanges)
        
        for i in range(len(data)):
            for j in range(len(data[i])-1):
                val = data[i][j]
                for idx in range(len(splitRanges[j])):
                    if val <= splitRanges[j][idx]:
                        data[i][j] = splitValues[idx]
                        break
                    if idx == len(splitRanges[j])-1:
                        data[i][j] = splitValues[idx+1]
                        break
        
        return data
    ##
    
    @staticmethod
    def discretizeInEqualRanges(data, count):
        splitStep = 1.0/float(count)
        splitValues = [val*splitStep for val in range(1, count+1)]
        splitRanges = []
        for attr in zip(*data)[:-1]:
            vMin = min(attr)
#             print "attr:", attr
#             print "vMin:", vMin
#             print "count:", count
            vRange = (max(attr)-vMin)/count
            #Warning: One element less:
            attrRanges = [vMin+val*vRange for val in range(1, count)]
            splitRanges.append(attrRanges)
        
        for i in range(len(data)):
            for j in range(len(data[i])-1):
                val = data[i][j]
                for idx in range(len(splitRanges[j])):
                    if val <= splitRanges[j][idx]:
                        data[i][j] = splitValues[idx]
                        break
                    if idx == len(splitRanges[j])-1:
                        data[i][j] = splitValues[idx+1]
                        break
        
        return data
    ##
    
    @staticmethod
    def splitByClass(data):
        clsSplit = {}
        for idx in range(len(data)):
            record = data[idx]
            clsName = record[-1]
            if clsName not in clsSplit:
                clsSplit[clsName] = []
            clsSplit[clsName].append(record)
        return clsSplit
    ##
    
    @staticmethod
    def splitForCV(data, folds=10, stratified=False): #Warning: Really inefficient implementation!
        dataFolds = []
        if stratified:
            # Split class specific records into required amount of folds:
            clsSplit = []
            for _, clsRecords in Dataset.splitByClass(data).items():
                clsSplit.append(Dataset._splitEqually(clsRecords, folds))
            
            # Diagnostic tool:
    #         print("[Folds count]: Records per fold count")
    #         for idx in range(len(clsSplit)):
    #             print("[%s]: %s" % (str(len(clsSplit[idx])), ", ".join([str(len(item)) for item in clsSplit[idx]])))
            
            # Sum class folds arrays into data folds:
            for clsFold in zip(*clsSplit):
                dtFold = []
                for item in clsFold:
                    dtFold += item
                random.shuffle(dtFold)
                dataFolds.append(dtFold)
        else:
            dt = list(data)
            random.shuffle(dt)
            dataFolds = Dataset._splitEqually(dt, folds)
        
        # Split data into training and test sets:
        cvData = []
        for i in range(len(dataFolds)):
            dTrain = []
            dTest = None
            for j in range(len(dataFolds)):
                if j == i:
                    dTest = dataFolds[j]
                    continue
                dTrain += dataFolds[j]
            cvData.append((dTrain, dTest))
        return cvData
    ##
    
    @staticmethod
    def _splitEqually(data, folds):
        foldSize = len(data)/float(folds)
        lastIdx = 0.0
        dataFolds = []
        while lastIdx < len(data):
            if int(lastIdx + 2*foldSize) > len(data):
                foldSize *= 2
            dataFolds.append(data[int(lastIdx):int(lastIdx + foldSize)])
            lastIdx += foldSize
        return dataFolds
##
